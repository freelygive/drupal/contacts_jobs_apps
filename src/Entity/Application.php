<?php

namespace Drupal\contacts_jobs_apps\Entity;

use Drupal\contacts_jobs\Entity\Job;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\name\NameFormatterInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Application entity.
 *
 * @ingroup contacts_jobs_apps
 *
 * @ContentEntityType(
 *   id = "cj_app",
 *   label = @Translation("Application"),
 *   label_plural = @Translation("Applications"),
 *   label_collection = @Translation("Applications"),
 *   bundle_label = @Translation("Application form"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contacts_jobs_apps\ApplicationListBuilder",
 *     "views_data" = "Drupal\contacts_jobs_apps\Entity\ApplicationViewsData",
 *     "form" = {
 *       "default" = "Drupal\contacts_jobs_apps\Form\JobApplicationForm",
 *       "apply" = "Drupal\contacts_jobs_apps\Form\JobApplicationForm",
 *       "apply_external" = "Drupal\contacts_jobs_apps\Form\JobExternalApplicationForm",
 *       "edit" = "Drupal\contacts_jobs_apps\Form\JobApplicationForm",
 *       "withdraw" = "Drupal\contacts_jobs_apps\Form\WithdrawForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\contacts_jobs_apps\ApplicationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_jobs_apps\ApplicationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cj_app",
 *   admin_permission = "administer job applications",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "owner" = "user_id",
 *   },
 *   links = {
 *     "collection" = "/admin/content/job-applications",
 *     "canonical" = "/job/{contacts_job}/applications/{cj_app}",
 *     "edit-form" = "/user/{user}/applications/{cj_app}/edit",
 *     "delete-form" = "/user/{user}/applications/{cj_app}/delete",
 *     "withdraw-form" = "/user/{user}/applications/{cj_app}/withdraw"
 *   },
 *   bundle_entity_type = "cj_app_type",
 *   field_ui_base_route = "entity.cj_app_type.edit_form"
 * )
 */
class Application extends ContentEntityBase implements EntityOwnerInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * The name formatter service.
   *
   * @var \Drupal\name\NameFormatterInterface|null
   */
  protected static $nameFormatter;

  /**
   * Get the name formatter, statically caching it.
   *
   * @return \Drupal\name\NameFormatterInterface|null
   *   The name formatter service, if available.
   */
  protected static function getNameFormatter(): ?NameFormatterInterface {
    if (!isset(self::$nameFormatter) && \Drupal::hasService('name.formatter')) {
      self::$nameFormatter = \Drupal::service('name.formatter');
    }
    return self::$nameFormatter;
  }

  /**
   * Creates a new application, for the specified job.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The job that will be associated with the application.
   *
   * @return \Drupal\contacts_jobs_apps\Entity\Application
   *   The new application.
   */
  public static function createForJob(JobInterface $job) {
    $application = Application::create([
      'type' => $job->hasField('external_ats') && !$job->get('external_ats')->isEmpty() ? 'external_ats' : 'default',
      'job' => $job->id(),
    ]);

    return $application;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'state' => 'draft',
      'user_id' => static::getDefaultEntityOwner(),
    ];

    // Copy values from the user.
    /** @var \Drupal\user\UserInterface|null $user */
    if ($values['user_id'] && $user = User::load($values['user_id'])) {
      if (!array_key_exists('email', $values)) {
        $values['email'] = $user->getEmail();
      }

      /** @var \Drupal\profile\Entity\ProfileInterface|null $profile */
      if (!array_key_exists('name', $values) && $profile = $user->get('profile_crm_indiv')->entity) {
        if ($profile->hasField('crm_name') && !$profile->get('crm_name')->isEmpty()) {
          $values['name'] = $profile->get('crm_name')->getValue();
        }
      }
    }

    \Drupal::moduleHandler()->invokeAll(
      $storage_controller->getEntityTypeId() . '_pre_create',
      [$storage_controller, &$values],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return new TranslatableMarkup('Application for @job from @applicant', [
      '@job' => $this->getJob()->label(),
      '@applicant' => $this->getFormattedName(),
    ]);
  }

  /**
   * Get the formatted name for the applicant.
   *
   * @return string
   *   The formatted name.
   */
  public function getFormattedName() {
    // If there is no name field or it's empty, use the user account or return
    // a static string indicating deleted.
    if (!$this->hasField('name') || $this->get('name')->isEmpty()) {
      $owner = $this->getOwner();
      return $owner ? $owner->label() : new TranslatableMarkup('deleted');
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->get('name')->first();

    // If this is a name item, format it.
    if ($item->getFieldDefinition()->getType() === 'name') {
      return self::getNameFormatter()
        ->format($item->getValue());
    }

    // Otherwise return the main property, if there is one.
    if ($main_property = $item::mainPropertyName()) {
      return $item->{$main_property};
    }

    // Finally return a static unknown string.
    return new TranslatableMarkup('Unknown');
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Whether the application has been submitted.
   *
   * @return bool
   *   Whether the application has been submitted.
   */
  public function isSubmitted() : bool {
    // If it's not draft, it's been submitted.
    return $this->state->value && $this->state->value != 'draft';
  }

  /**
   * Gets the job for this application.
   *
   * @return \Drupal\contacts_jobs\Entity\Job|null
   *   The job.
   */
  public function getJob() : ?Job {
    return $this->get('job')->entity;
  }

  /**
   * The organisation associated with this application.
   *
   * @return \Drupal\user\UserInterface|null
   *   The organisation associated with this application.
   */
  public function getOrg() : ?UserInterface {
    if ($job = $this->getJob()) {
      return $job->get('organisation')->entity;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $parameters = parent::urlRouteParameters($rel);
    if ($rel === 'canonical') {
      $parameters['contacts_job'] = $this->get('job')->target_id;
    }
    else {
      $parameters['user'] = $this->get('user_id')->target_id;
    }
    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('name')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(new TranslatableMarkup('Email'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Application Status'))
      ->setDescription(t('The application state.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setSetting('workflow', 'contacts_jobs_apps_application_process')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'state_transition_form',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['job'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Job'))
      ->setSetting('target_type', 'contacts_job')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['status_log'] = BaseFieldDefinition::create('status_log')
      ->setLabel(t('Status History'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('A log of when the status was changed and by whom.'))
      ->setSettings([
        'max_length' => 50,
        'source_field' => 'state',
      ])
      ->setDisplayOptions('view', [
        'weight' => 20,
        'type' => 'status_log_list',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['date_submitted'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date submitted'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Retrieves a collection of transition IDs that the user can apply manually.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return array
   *   Array of transition IDs that the user can invoke.
   */
  public function getManuallyAllowedTransitions(AccountInterface $account) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->state->first();
    $transitions = array_keys($state->getTransitions());

    $staff_only_transitions = [
      'submit',
      'withdraw',
      'reset_to_not_submitted',
    ];

    if ($account->hasPermission('administer job applications')) {
      return array_intersect($transitions, $staff_only_transitions);
    }

    return [];
  }

  /**
   * Sets the submitted date.
   *
   * @param \DateTimeInterface $date
   *   The date.
   */
  public function setSubmittedDate(\DateTimeInterface $date) {
    $date = clone $date;
    $formatted = $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
      ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $this->set('date_submitted', $formatted);
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner() {
    // Only use the current user if they are authenticated. Otherwise apps get
    // linked to the anonymous user which is unhelpful.
    $current_user = \Drupal::currentUser();
    return $current_user->isAuthenticated() ? $current_user->id() : NULL;
  }

}
