<?php

namespace Drupal\contacts_jobs_apps\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Application entities.
 */
class ApplicationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['cj_app']['state']['filter']['id'] = 'state_machine_state';

    return $data;
  }

}
