<?php

namespace Drupal\contacts_jobs_apps\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Application type entity.
 *
 * @ConfigEntityType(
 *   id = "cj_app_type",
 *   label = @Translation("Application Form"),
 *   label_plural = @Translation("Application Forms"),
 *   label_collection = @Translation("Application Forms"),
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_jobs_apps\ApplicationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\contacts_jobs_apps\Form\ApplicationTypeForm",
 *       "edit" = "Drupal\contacts_jobs_apps\Form\ApplicationTypeForm",
 *       "delete" = "Drupal\contacts_jobs_apps\Form\ApplicationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cj_app_type",
 *   admin_permission = "administer job applications",
 *   bundle_of = "cj_app",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/apps/forms/add",
 *     "edit-form" = "/admin/structure/apps/forms/{cj_app_type}",
 *     "delete-form" = "/admin/structure/apps/forms/{cj_app_type}/delete",
 *     "collection" = "/admin/structure/apps/forms"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class ApplicationType extends ConfigEntityBundleBase {

  /**
   * The application form id.
   *
   * @var string
   */
  protected $id;

  /**
   * The application form name.
   *
   * @var string
   */
  protected $label;

}
