<?php

namespace Drupal\contacts_jobs_apps\Plugin\QueueWorker;

use Drupal\contacts_communication\BuildAndSendCommunicationTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue worker to notify applicants that their application will expire soon.
 *
 * @QueueWorker(
 *   id = "contacts_jobs_apps_notify_deadline_approaching",
 *   title = @Translation("Notify applicants when the application deadline is approaching."),
 *   cron = {"time" = 60},
 * )
 *
 * @package Drupal\contacts_jobs_apps\Plugin\QueueWorker
 */
class NotifyApplicationDeadlineApproaching extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  use BuildAndSendCommunicationTrait;

  /**
   * Application storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $applicationStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('cj_app')
    );
    $instance->setLoggerFactory($container->get('logger.factory'));
    $instance->setOperationManager($container->get('plugin.manager.communication.operation'));
    $instance->setBuilderManager($container->get('plugin.manager.entity_template.builder'));
    return $instance;
  }

  /**
   * NotifyApplicationDeadlineApproaching constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $application_storage
   *   The application storage.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityStorageInterface $application_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->applicationStorage = $application_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    foreach ($this->applicationStorage->loadMultiple($data) as $application) {
      $this->buildAndSendCommunication(
        ['application' => $application],
        'config:ap3_app_deadline_approaching_email'
      );
    }
  }

}
