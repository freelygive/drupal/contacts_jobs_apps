<?php

namespace Drupal\contacts_jobs_apps\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue worker to find and queue cancelled_pending subscription notifications.
 *
 * @QueueWorker(
 *   id = "contacts_jobs_apps_queue_notify_deadline_approaching",
 *   title = @Translation("Queue notifications of when application deadlines apprach."),
 *   cron = {"time" = 60},
 *   cron_invoke = {},
 *   item_worker = "contacts_jobs_apps_notify_deadline_approaching"
 * )
 *
 * @package Drupal\contacts_jobs_subscriptions\Plugin\QueueWorker
 */
class QueueNotifyApplicationDeadlineApproaching extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The queue to add items to.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Application storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $applicationStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue')->get($plugin_definition['item_worker']),
      $container->get('entity_type.manager')->getStorage('cj_app')
    );
  }

  /**
   * QueueNotifyApplicationDeadlineApproaching constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   The queue to add items to.
   * @param \Drupal\Core\Entity\EntityStorageInterface $application_storage
   *   The application entity storage.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    QueueInterface $queue,
    EntityStorageInterface $application_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->queue = $queue;
    $this->applicationStorage = $application_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\Core\Datetime\DrupalDateTime|null $last_run */
    /** @var \Drupal\Core\Datetime\DrupalDateTime $cron_time */
    [$last_run, $cron_time] = $data;
    $week = new \DateInterval('P7D');

    $query = $this->applicationStorage->getQuery();
    $query->condition('state', 'draft');
    $query->condition('job.entity.closing', $cron_time->add($week)->format('U'), '<=');
    if ($last_run) {
      $query->condition('job.entity.closing', $last_run->add($week)->format('U'), '>');
    }

    foreach (array_chunk($query->execute(), 10) as $data) {
      $this->queue->createItem($data);
    }
  }

}
