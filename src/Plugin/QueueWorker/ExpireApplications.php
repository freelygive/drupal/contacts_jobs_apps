<?php

namespace Drupal\contacts_jobs_apps\Plugin\QueueWorker;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'contacts_jobs_apps_expire' queue worker.
 *
 * @QueueWorker(
 *   id = "contacts_jobs_apps_expire",
 *   title = @Translation("Expire applications"),
 *   cron = {"time" = 60},
 *   cron_invoke = {
 *     "interval" = "i",
 *   }
 * )
 */
class ExpireApplications extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The job entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $jobStorage;

  /**
   * The application entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $appStorage;

  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    $entity_type_manager = $container->get('entity_type.manager');
    $plugin->jobStorage = $entity_type_manager->getStorage('contacts_job');
    $plugin->appStorage = $entity_type_manager->getStorage('cj_app');
    $plugin->queue = $container->get('queue')->get($plugin_id);
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\Core\Datetime\DrupalDateTime|null $last_run */
    /** @var \Drupal\Core\Datetime\DrupalDateTime $cron_time */
    /** @var int[]|null $expire_draft_jobs */
    /** @var int[]|null $expire_submitted_jobs */
    [$last_run, $cron_time, $expire_draft_jobs, $expire_submitted_jobs] = $data;

    // Draft applications should be expired for all closed or unpublished jobs.
    if (!isset($expire_draft_jobs)) {
      $expire_draft_jobs = $this->getJobIdsToExpire($last_run, $cron_time);
    }

    // Submitted (unread) applications should be expired 30 days after closed or
    // unpublished.
    if (!isset($expire_submitted_jobs)) {
      $interval = new \DateInterval('P30D');
      $expire_submitted_jobs = $this->getJobIdsToExpire(
        $last_run ? (clone $last_run)->sub($interval) : NULL,
        (clone $cron_time)->sub($interval),
      );
    }

    // Nothing to do if both sets are empty. No need to re-queue.
    if (empty($expire_draft_jobs) && empty($expire_submitted_jobs)) {
      return;
    }

    // Get hold of all the applications to expire.
    $query = $this->appStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->range(0, 20);

    // Combine our job IDs with the application state.
    $or = $query->orConditionGroup();

    // Draft applications.
    if (!empty($expire_draft_jobs)) {
      $or->condition(
        $query->andConditionGroup()
          ->condition('state', 'draft')
          ->condition('job', $expire_draft_jobs, 'IN')
      );
    }

    // Submitted applications.
    if (!empty($expire_submitted_jobs)) {
      $or->condition(
        $query->andConditionGroup()
          ->condition('state', 'submitted')
          ->condition('job', $expire_submitted_jobs, 'IN')
      );
    }
    $query->condition($or);

    $app_ids = $query->execute();

    /** @var \Drupal\contacts_jobs_apps\Entity\Application $app */
    foreach ($this->appStorage->loadMultiple($app_ids) as $app) {
      /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
      $state = $app->get('state')->first();

      // Check that the expire transition is allowed.
      $transitions = $state->getTransitions();
      if (isset($transitions['expire'])) {
        $state->applyTransition($transitions['expire']);
        $app->save();
      }
    }

    // If we had our batch size of applications, requeue as there may be more.
    if (count($app_ids) === 20) {
      // Rebuild data to ensure the draft/submitted job IDs are included to
      // avoid having to re-run those queries on every execute.
      $this->queue->createItem([
        $last_run,
        $cron_time,
        $expire_draft_jobs,
        $expire_submitted_jobs,
      ]);
    }
  }

  /**
   * Get job IDs to expire based on closing/un-publishing between given dates.
   *
   * @param \Drupal\Component\Datetime\DateTimePlus|null $start
   *   The start time, or NULL if we should look back at all time.
   * @param \Drupal\Component\Datetime\DateTimePlus $end
   *   The end time.
   *
   * @return array
   *   An array of job IDs.
   */
  protected function getJobIdsToExpire(?DateTimePlus $start, DateTimePlus $end) {
    $query = $this->jobStorage
      ->getQuery()
      ->accessCheck(FALSE);

    // If we have a start, time is between the start and end.
    if ($start) {
      $times = [$start->getTimestamp(), $end->getTimestamp()];
      $operator = 'BETWEEN';
    }
    // Otherwise it's just before the end.
    else {
      $times = $end->getTimestamp();
      $operator = '<=';
    }

    // Apply the filter to both publish_end and closing times.
    $or = $query->orConditionGroup();
    $or->condition('publish_end', $times, $operator);
    $or->condition('closing', $times, $operator);
    $query->condition($or);

    return $query->execute();
  }

}
