<?php

namespace Drupal\contacts_jobs_apps;

use Drupal\contacts_jobs_apps\Entity\Application;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Application entity.
 *
 * @see \Drupal\contacts_jobs_apps\Entity\Application.
 */
class ApplicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    assert($entity instanceof Application);
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $entity->get('state')->first();

    // Check the administrative permission.
    $result = AccessResult::allowedIfHasPermission($account, 'administer job applications');

    // External applications can only be deleted.
    if ($entity->bundle() === 'external_ats') {
      $result = $result->orIf(
        AccessResult::forbiddenIf($operation !== 'delete')
          ->addCacheableDependency($entity)
      );
      if ($result->isForbidden()) {
        return $result;
      }
    }

    // If you don't have administrative permissions, nothing can be done with
    // a withdrawn job.
    if (!$result->isAllowed()) {
      $result = $result->orIf(AccessResult::forbiddenIf($state->getId() === 'withdrawn')
        ->addCacheableDependency($entity));
    }

    // For transition operations, check the transition is allowed.
    if (in_array($operation, ['withdraw'])) {
      $allowed_transitions = array_keys($state->getTransitions());
      $result = $result->orIf(AccessResult::forbiddenIf(!in_array($operation, $allowed_transitions))
        ->addCacheableDependency($entity));
    }

    // View is recruiter only for submitted/received applications, so check the
    // account has permissions on the organisation and the state.
    if ($operation === 'view' && $org = $entity->getOrg()) {
      /** @var \Drupal\group\Entity\GroupInterface $group */
      $group = $org->get('group')->entity;
      $member = $group->getMember($account);
      if ($member) {
        $group_access = AccessResult::allowedIf($member->hasPermission('manage organisation jobs'))
          ->addCacheableDependency($member)
          ->addCacheContexts(['user']);
        // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
        $group_access->andIf(AccessResult::allowedIf(in_array($state->getId(), ['submitted', 'received']))
          ->addCacheableDependency($entity));
        $result = $result->orIf($group_access);
      }
      else {
        $result = $result->orIf(AccessResult::neutral()->addCacheContexts(['user']));
      }
    }
    // Update and withdraw are owner only, so check the ownership of the app.
    elseif (in_array($operation, ['update', 'withdraw'])) {
      $owner_result = AccessResult::allowedIf($entity->getOwnerId() === $account->id())
        ->addCacheableDependency($entity)
        ->addCacheContexts(['user']);

      // Updates are only allowed in the draft state and if the owner has the
      // add application permission.
      if ($operation === 'update') {
        // We use an andIf as this only applies to the owner check.
        $owner_result = $owner_result->andIf(AccessResult::allowedIf($state->getId() === 'draft')
          ->addCacheableDependency($entity));

        // We use an andIf, as both owner and permission must be allowed.
        $owner_result = $owner_result->andIf(AccessResult::allowedIfHasPermission($account, 'add job applications'));
      }

      // Add the owner result with an OR (against the administrative
      // permission).
      $result = $result->orIf($owner_result);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job applications');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if (in_array($field_definition->getName(), ['status', 'status_log'])) {
      return AccessResult::allowedIfHasPermission($account, 'administer job applications');
    }

    // Fall back to the defaults.
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
