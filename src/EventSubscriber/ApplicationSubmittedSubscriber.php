<?php

namespace Drupal\contacts_jobs_apps\EventSubscriber;

use Drupal\communication\Entity\Communication;
use Drupal\communication\OperationPluginManager;
use Drupal\contacts_jobs_apps\Entity\Application;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\entity_template\TemplateBuilderManager;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber triggered when application is submitted.
 *
 * @package Drupal\contacts_jobs_apps\EventSubscriber
 */
class ApplicationSubmittedSubscriber implements EventSubscriberInterface {

  /**
   * The builder manager.
   *
   * @var \Drupal\entity_template\TemplateBuilderManager
   */
  protected $builderManager;

  /**
   * The operation manager.
   *
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * ApplicationSubmittedSubscriber constructor.
   *
   * @param \Drupal\entity_template\TemplateBuilderManager $builder_manager
   *   Builder_manager.
   * @param \Drupal\communication\OperationPluginManager $operation_manager
   *   Operation_manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger_factory.
   */
  public function __construct(
    TemplateBuilderManager $builder_manager,
    OperationPluginManager $operation_manager,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->builderManager = $builder_manager;
    $this->operationManager = $operation_manager;
    $this->logger = $logger_factory->get('contacts_jobs_apps');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['contacts_jobs_apps_applications.submit.pre_transition'][] = ['setSubmittedTime'];
    $events['contacts_jobs_apps_applications.submit_external_ats.pre_transition'][] = ['setSubmittedTime'];
    $events['contacts_jobs_apps_applications.submit.post_transition'][] = ['notifyCandidate'];
    $events['contacts_jobs_apps_applications.submit.post_transition'][] = ['notifyRecruiter'];
    return $events;
  }

  /**
   * Sets the submitted time on the application.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Event.
   */
  public function setSubmittedTime(WorkflowTransitionEvent $event) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $app */
    $app = $event->getEntity();
    $app->setSubmittedDate(new \DateTime());
  }

  /**
   * Notify the candidate that the application has been submitted.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\entity_template\Exception\MultipleAvailableBlueprintException
   */
  public function notifyCandidate(WorkflowTransitionEvent $event) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $app */
    $app = $event->getEntity();
    $this->buildAndSendTemplate(
      'config:candidate_application_notification_email',
      $app,
      $app->getOwner()
    );
  }

  /**
   * Notify the recruiter that the application has been submitted.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\entity_template\Exception\MultipleAvailableBlueprintException
   */
  public function notifyRecruiter(WorkflowTransitionEvent $event) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $app */
    $app = $event->getEntity();
    $this->buildAndSendTemplate(
      'config:recruiter_application_notification_email',
      $app,
      $app->getJob()->getOwner()
    );
  }

  /**
   * Build and send a particular builder for order notifications.
   *
   * @param string $build_id
   *   The builder id.
   * @param \Drupal\contacts_jobs_apps\Entity\Application $application
   *   The application.
   * @param \Drupal\user\UserInterface|null $user
   *   The user to notify.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\entity_template\Exception\MultipleAvailableBlueprintException
   */
  protected function buildAndSendTemplate(string $build_id, Application $application, UserInterface $user = NULL) {
    /** @var \Drupal\entity_template\Plugin\EntityTemplate\Builder\BuilderInterface $builder */
    $builder = $this->builderManager->createInstance($build_id);

    $params = [];
    if ($builder->getContextDefinition('application')) {
      $params['application'] = $application;
    }

    $result = $builder->execute($params);
    foreach ($result->getItems() as $item) {
      // If the builder has build something that isn't a communication, just
      // save it an move on.
      if (!$item instanceof Communication) {
        $item->save();
        continue;
      }

      // Attempt to send the communication.
      $available_operations = $this->operationManager->availableOperations($item);
      if (!isset($available_operations['send'])) {
        $item->save();
        continue;
      }

      $op = $available_operations['send'];
      if ($op->validate($item)) {
        try {
          $op->run($item);
        }
        catch (PluginException $exception) {
          $item->save();
          $this->logger->warning('Failed to send Communication ' . $item->id() . ': ' . $exception->getMessage());
        }
      }
      else {
        $item->save();
        $this->logger->warning('Failed to validate Communication ' . $item->id());
      }
    }
  }

}
