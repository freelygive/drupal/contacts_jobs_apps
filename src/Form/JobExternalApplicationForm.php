<?php

namespace Drupal\contacts_jobs_apps\Form;

use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class JobExternalApplicationForm extends JobApplicationForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $job = $this->entity->getJob();
    $form = parent::buildForm($form, $form_state);

    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => ['card', 'card-body', 'mb-3']],
      '#weight' => -999
    ];

    $form['description']['intro'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t(
        'Applications for %job with %org are handled via an external application tracking system.',
        [
          '%job' => $job->label(),
          '%org' => $job->get('organisation')->entity->label(),
        ],
      ),
    ];

    $form['description']['disclaimer'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => ['class' => ['text-muted']],
      '#value' => $this->t(
        '@sitename is not responsible for the contents of the external application system or the security and privacy of any data you submit to it.',
        [
          '@sitename' => $this->config('system.site')->get('name'),
        ]
      ),
    ];

    // Make the form submit to a new tab so the candidate is still on the
    // website then redirect this page to the job listing page. Only do this if
    // we are not already processing input, as in that case we are already in a
    // new tab.
    if (!$form_state->isProcessingInput()) {
      $form['#attributes']['target'] = 'external_ats';
      $form['#attributes']['onsubmit'] = "window.open('about:blank','external_ats');window.location='{$job->toUrl()->toString()}'";
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#access'] = FALSE;
    $actions['complete']['#value'] = $this->t('Continue to external application');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function complete(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $this->entity->get('state')->first();
    $state->applyTransitionById('submit_external_ats');
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    // Use a trusted redirect response to redirect.
    $url = $this->entity->getJob()->get('external_ats')->first()->getUrl();
    $response = new TrustedRedirectResponse($url->toString());
    $response->getCacheableMetadata()->setCacheMaxAge(0);
    $form_state->setResponse($response);

    return $result;
  }

}
