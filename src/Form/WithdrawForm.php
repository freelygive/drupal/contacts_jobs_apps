<?php

namespace Drupal\contacts_jobs_apps\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a confirmation form for withdrawing applications.
 */
class WithdrawForm extends ContentEntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\contacts_jobs_apps\Entity\Application
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to withdraw your application to %label?', [
      '%label' => $this->entity->getJob()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Withdraw');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $this->entity->get('state')->first();

    $workflow = $state->getWorkflow();
    $transitions = $workflow->getAllowedTransitions($state->value, $this->entity);

    if (!isset($transitions['withdraw'])) {
      return;
    }

    $state->applyTransition($transitions['withdraw']);
    $this->entity->save();
  }

}
