<?php

namespace Drupal\contacts_jobs_apps\Form;

use Drupal\contacts_jobs_apps\Entity\Application;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form for applying to a job.
 */
class JobApplicationForm extends ContentEntityForm {

  /**
   * The flood key for acquisition attempts.
   */
  protected const FLOOD_KEY = 'contacts_jobs_apps.acquisitions';

  /**
   * The acquisition service.
   *
   * @var \Drupal\decoupled_auth\AcquisitionServiceInterface
   */
  protected $acquisition;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->routeMatch = $container->get('current_route_match');
    $form->acquisition = $container->get('decoupled_auth.acquisition');
    $form->flood = $container->get('flood');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter($entity_type_id) !== NULL) {
      /** @var \Drupal\contacts_jobs_apps\Entity\Application $entity */
      $entity = $route_match->getParameter($entity_type_id);
    }
    elseif ($job = $this->getRouteMatch()->getParameter('contacts_job')) {
      $entity = Application::createForJob($job);
    }
    else {
      throw new NotFoundHttpException('Unable to find job or application');
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#restrict_user_fields'] = FALSE;
    if ($this->entity->getOwnerId()) {
      $form['#cache']['contexts'][] = 'user.permissions';
      $entity_type = $this->entity->getEntityType();
      if (!$this->currentUser()->hasPermission($entity_type->getAdminPermission())) {
        $form['#restrict_user_fields'] = TRUE;
      }
    }

    $form = parent::buildForm($form, $form_state);
    if ($form['#restrict_user_fields']) {
      foreach (['email', 'name'] as $field_name) {
        if (isset($form[$field_name]) && !$this->entity->get($field_name)->isEmpty()) {
          $form[$field_name]['#disabled'] = TRUE;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $application */
    $application = parent::buildEntity($form, $form_state);

    // If we don't have an owner, run acquisitions on the email address.
    if (!$application->getOwnerId()) {
      $email = $application->get('email')->value;
      $user = $this->acquisition->acquire(
        ['mail' => $email],
        ['name' => 'contacts_jobs_apps_guest_app'],
        $method,
      );

      // If creating a user, add the relevant roles.
      if ($method === 'create') {
        $user->setEmail($email);
        $user->addRole('crm_indiv');
        $user->addRole('candidate');
        $application->setOwner($user);
      }
      // If we have a user attach it. Validation will ensure the user is a
      // candidate.
      elseif ($user) {
        $application->setOwner($user);
      }
      // This scenario shouldn't happen, so log an error. Validation will show
      // an error to the user.
      else {
        $this->logger('contacts_jobs_apps')->error("Failed to acquire or create user for {$email}.");
      }
    }

    return $application;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $entity */
    $entity = parent::validateForm($form, $form_state);

    // Ensure that there is a valid owner for the application for the case of
    // acquisitions.
    if (!$this->currentUser()->isAuthenticated()) {
      // Use flood to prevent sniffing registered email addresses. Trying 10
      // different email addresses in a 5 minute window seems a reasonable limit
      // for suspicious behaviour.
      if (!$this->flood->isAllowed(self::FLOOD_KEY, 10, 300)) {
        $form_state->setError($form, $this->t('Sorry, something unexpected went wrong. Please wait and try again later.'));
        $this->logger('contacts_jobs_apps')->warning('Application acquisition flood limit hit.');
      }
      else {
        $owner = $entity->getOwner();
        if ($owner === NULL) {
          $form_state->setError($form, $this->t('Sorry, something unexpected went wrong. Please try again later.'));
        }
        elseif (!$owner->hasRole('candidate')) {
          $form_state->setError(
            $form['email']['widget'][0]['value'],
            $this->t(
              'This email address is already in use by an account that cannot apply for jobs. Please try a different email or <a href="@support">contact support</a>.',
              ['@support' => 'mailto:hello@fmcgjobs.com']
            ),
          );
        }
      }

      // Always register a flood event so continuing despite being blocked will
      // keep the flood active.
      $this->flood->register(self::FLOOD_KEY, 300);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $app */
    $app = $this->entity;

    $actions = parent::actions($form, $form_state);

    if ($app->get('state')->value === 'draft') {
      $actions['submit']['#value'] = $this->t('Save draft');
      $actions['complete'] = $actions['submit'];
      $actions['complete']['#value'] = $this->t('Complete application');
      array_splice(
        $actions['complete']['#submit'],
        array_search('::save', $actions['complete']['#submit']),
        0,
        '::complete',
      );
    }

    $actions['submit']['#access'] = $this->currentUser()->isAuthenticated();

    return $actions;
  }

  /**
   * Submission handler to submit the application.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function complete(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $this->entity->get('state')->first();
    $state->applyTransitionById('submit');
    $this->messenger()->addStatus($this->t(
      'Your application for %job has been submitted.',
      ['%job' => $this->entity->getJob()->label()],
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $owner = $this->entity->getOwner();

    if ($this->currentUser()->isAuthenticated()) {
      $form_state->setRedirect(
        'contacts_jobs_apps.user.job_apps',
        ['user' => $this->entity->getOwnerId()],
      );
    }
    else {
      $form_state->setRedirectUrl($this->entity->getJob()->toUrl());
      $query = [
        'register' => 'candidate',
        'app' => $this->entity->uuid(),
      ];
      $this->messenger()->addWarning($this->t(
        'Please <a href="@url">register<a/> with %email to view your application and its status.',
        [
          '@url' => Url::fromRoute(
            'user.register',
            [],
            ['query' => $query],
          )->toString(),
          '%email' => $owner->getEmail(),
        ],
      ));
    }

    // Copy name onto the user's profile if it isn't already set.
    if ($this->entity->hasField('name') && !$this->entity->get('name')->isEmpty()) {
      /** @var \Drupal\profile\Entity\ProfileInterface $profile */
      $profile = $owner->get('profile_crm_indiv')->entity ??
        $this->entityTypeManager
          ->getStorage('profile')
          ->create(['type' => 'crm_indiv'])->setOwner($owner);
      if ($profile->hasField('crm_name') && $profile->get('crm_name')->isEmpty()) {
        $profile->set('crm_name', $this->entity->get('name')->getValue());
        $profile->save();
      }
    }

    // Store the application on form state for submit handlers added via alters.
    $form_state->set('cj_app', $this->entity);

    return $result;
  }

}
