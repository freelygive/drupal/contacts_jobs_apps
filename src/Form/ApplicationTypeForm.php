<?php

namespace Drupal\contacts_jobs_apps\Form;

use Drupal\contacts_jobs_apps\Entity\ApplicationType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Allows for adding/editing application forms.
 */
class ApplicationTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\contacts_jobs_apps\Entity\ApplicationType $job_form */
    $job_form = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $job_form->label(),
      '#description' => $this->t("Label for the Application type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $job_form->id(),
      '#machine_name' => [
        'exists' => [ApplicationType::class, 'load'],
      ],
      '#disabled' => !$job_form->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_jobs_apps\Entity\ApplicationType $job_form */
    $job_form = $this->entity;
    $status = $job_form->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label form.', [
          '%label' => $job_form->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label form.', [
          '%label' => $job_form->label(),
        ]));
    }
    $form_state->setRedirectUrl($job_form->toUrl('collection'));
  }

}
