<?php

namespace Drupal\contacts_jobs_apps;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Application entities.
 *
 * @ingroup contacts_jobs_apps
 */
class ApplicationListBuilder extends EntityListBuilder {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => $this->t('#'),
      'applicant' => $this->t('Applicant'),
      'job' => $this->t('Job'),
      'org' => $this->t('Organisation'),
      'closing' => $this->t('Deadline'),
      'status' => $this->t('Status'),
      'submitted' => $this->t('Submitted'),
    ] + parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $entity */
    $row = [];

    $row['id'] = $entity->id();

    if ($owner = $entity->getOwner()) {
      $row['applicant'] = Link::createFromRoute(
        $entity->getFormattedName(),
        'contacts.contact',
        ['user' => $entity->getOwnerId()],
      );
    }
    else {
      $row['applicant'] = $entity->getFormattedName();
    }

    $job = $entity->getJob();
    $row['job'] = $job ? $job->toLink() : $this->t('No job found');

    if ($org = $entity->getOrg()) {
      $row['org'] = Link::createFromRoute(
        $org->label(),
        'contacts.contact',
        ['user' => $org->id()],
      );
    }
    else {
      $row['org'] = $this->t('No org found');
    }

    $row['closing'] = NULL;
    if ($job) {
      $row['closing'] = $this->dateFormatter->format($job->getClosingTime(), 'short');
    }

    $row['status'] = $entity->get('state')->first()->getLabel();

    $row['submitted'] = NULL;
    if ($entity->isSubmitted() && $date = $entity->get('date_submitted')->date) {
      $row['submitted'] = $this->dateFormatter->format($date->getTimestamp(), 'short');
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\contacts_jobs_apps\Entity\Application $entity */
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'url' => $entity->toUrl(),
        'weight' => 1,
      ];
    }

    if ($entity->access('withdraw')) {
      $operations['withdraw'] = [
        'title' => $this->t('Withdraw'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('withdraw-form')),
      ];
    }

    return $operations;
  }

}
