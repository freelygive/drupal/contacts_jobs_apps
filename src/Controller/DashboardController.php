<?php

namespace Drupal\contacts_jobs_apps\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The dashboard controller.
 */
class DashboardController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new DirectoryController object.
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityFormBuilder $entity_form_builder, AccountProxyInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFormBuilder = $entity_form_builder;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('current_user')
    );
  }

  /**
   * Title callback for the my jobs page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The applicant user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function jobsTitle(UserInterface $user) {
    return $this->t('Jobs for @label', ['@label' => $user->label()]);
  }

  /**
   * Page callback for applicants reviewing their applications.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The job applicant user.
   *
   * @return array
   *   A page showing job applications.
   */
  public function myJobsPage(AccountInterface $user) {
    $content = [];

    $members_view_block = \Drupal::service('plugin.manager.block')->createInstance('views_block:user_dashboard_job_apps-jobs_full');

    $block_content = $members_view_block->build();
    $content['jobs'] = [
      '#theme' => 'block',
      '#attributes' => [],
      '#configuration' => $members_view_block->getConfiguration(),
      '#plugin_id' => $members_view_block->getPluginId(),
      '#base_plugin_id' => $members_view_block->getBaseId(),
      '#derivative_plugin_id' => $members_view_block->getDerivativeId(),
      '#weight' => $members_view_block->getConfiguration()['weight'] ?? 0,
      'content' => $block_content,
    ];

    return $content;
  }

}
