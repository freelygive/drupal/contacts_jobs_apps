<?php

namespace Drupal\contacts_jobs_apps\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\HtmlEntityFormController;
use Drupal\Core\Http\Exception\CacheableAccessDeniedHttpException;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Jobboard Applications routes.
 */
class JobApplicationController extends HtmlEntityFormController implements ContainerInjectionInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $controller = new static(
      $container->get('http_kernel.controller.argument_resolver'),
      $container->get('form_builder'),
      $container->get('entity_type.manager')
    );
    $controller->currentUser = $container->get('current_user');
    $controller->requestStack = $container->get('request_stack');
    return $controller;
  }

  /**
   * Route title callback for the apply route.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $contacts_job
   *   The job being applied for.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The page title.
   */
  public function applyTitle(JobInterface $contacts_job): MarkupInterface {
    return new TranslatableMarkup('Apply for @job', [
      '@job' => $contacts_job->label(),
    ]);
  }

  /**
   * Handle application requests.
   */
  public function apply(JobInterface $contacts_job, Request $request, RouteMatchInterface $route_match) {
    $cacheability = new CacheableMetadata();

    // Check view access on the job.
    $access = $contacts_job->access('view', $this->currentUser, TRUE);
    $cacheability->addCacheableDependency($access);
    if (!$access->isAllowed()) {
      throw new CacheableAccessDeniedHttpException(
        $cacheability,
        $access instanceof AccessResultReasonInterface ? $access->getReason() : 'You do not have access to this job.',
      );
    }

    $cacheability->addCacheableDependency($contacts_job);

    // Check the job is open for applications.
    if (!$contacts_job->isPublished()) {
      throw new CacheableAccessDeniedHttpException($cacheability, 'Cannot apply for unpublished jobs.');
    }

    // Everything else varies depending on the specific user and whether we want
    // guest access.
    $cacheability->addCacheableDependency($this->currentUser);
    $cacheability->addCacheContexts(['url.query_args:destination']);

    /** @var \Drupal\contacts_jobs_apps\Entity\Application|null $app */
    $app = NULL;

    // If we have a user, look for an existing application.
    if ($this->currentUser->isAuthenticated()) {
      $app_storage = $this->entityTypeManager
        ->getStorage('cj_app');
      $app_ids = $app_storage
        ->getQuery()
        ->condition('job', $contacts_job->id())
        ->condition('user_id', $this->currentUser->id())
        ->condition('state', 'withdrawn', '!=')
        ->range(0, 1)
        ->execute();


      $app = $app_ids ? $app_storage->load(reset($app_ids)) : NULL;
    }
    // Redirect to the Login page if this is not explicitly a guest application.
    elseif (!$this->requestStack->getCurrentRequest()->query->has('guest')) {
      $url = Url::fromRoute(
        'user.login',
        [],
        [
          'query' => [
            'destination' => Url::fromRoute('<current>')->toString(),
          ],
        ],
      );
      return $this->prepareResponse(new RedirectResponse($url->toString()), $cacheability);
    }

    // Make sure we check the appropriate access for creating an application.
    if (!$app) {
      $access = $this->entityTypeManager
        ->getAccessControlHandler('cj_app')
        ->createAccess(NULL, $this->currentUser, [], TRUE);
      $cacheability->addCacheableDependency($access);
      if (!$access->isAllowed()) {
        throw new CacheableAccessDeniedHttpException(
          $cacheability,
          $access instanceof AccessResultReasonInterface ? $access->getReason() : 'You do not have access to apply.',
        );
      }
    }

    // Check whether this is an external ATS application.
    $is_external_ats = $contacts_job->hasField('external_ats') && !$contacts_job->get('external_ats')->isEmpty();

    // If there is an existing app, redirect to it or show the current status.
    if ($app) {
      /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
      $state = $app->get('state')->first();

      // Redirect to draft applications.
      if ($state->value === 'draft') {
        $response = new RedirectResponse($app->toUrl('edit-form')->toString());
        return $this->prepareResponse($response, $cacheability);
      }

      // Otherwise show a message.
      $response['#theme'] = 'contacts_jobs_apps_message';

      // External ATS shows information with a link to the ATS.
      if ($is_external_ats) {
        /** @var \Drupal\Core\Url $url */
        $ats_url = $contacts_job->get('external_ats')->first()->getUrl();
        $ats_url->setOption('attributes', ['target' => '_blank']);

        $response['message'] = [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => new TranslatableMarkup('Your application for %job with %org is handled via an external application tracking system.', [
            '%job' => $contacts_job->label(),
            '%org' => $contacts_job->get('organisation')->entity->label(),
          ]),
        ];

        $response['link_wrapper'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#attributes' => [
            'class' => ['my-3', 'text-center'],
          ],
        ];

        $response['link_wrapper']['button'] = [
          '#type' => 'link',
          '#url' => $ats_url,
          '#title' => new TranslatableMarkup('Visit application tracking system'),
          '#options' => [
            'attributes' => ['class' => ['btn', 'btn-secondary']],
          ],
        ];
      }
      // Internal applications just show a status.
      else {
        $response['message'] = [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => new TranslatableMarkup('Your application for %job with %org has been @status', [
            '%job' => $contacts_job->label(),
            '%org' => $contacts_job->get('organisation')->entity->label(),
            '@status' => strtolower($state->getLabel()),
          ]),
        ];
      }

      return $this->prepareResponse($response, $cacheability);
    }

    // If this is an external ATS application, swap out the form in use.
    if ($is_external_ats) {
      $route_object = clone $route_match->getRouteObject();
      $route_object->setDefault('_entity_form', 'cj_app.apply_external');
      $route_match = new RouteMatch(
        $route_match->getRouteName(),
        $route_object,
        $route_match->getParameters()->all(),
        $route_match->getRawParameters()->all(),
      );
    }

    // Otherwise, return the create form.
    $response = $this->getContentResult($request, $route_match);
    return $this->prepareResponse($response, $cacheability);
  }

  /**
   * Prepare a response by adding the appropriate cacheability.
   *
   * @param \Symfony\Component\HttpFoundation\Response|array $response
   *   Either a response object or a render array.
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheability
   *   The cachebility for the response.
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   The prepared response with cacheability.
   */
  protected function prepareResponse($response, CacheableMetadata $cacheability) {
    // Merge in our cacheability and response.
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency($cacheability);
    }
    elseif (is_array($response)) {
      $cacheability
        ->merge(CacheableMetadata::createFromRenderArray($response))
        ->applyTo($response);
    }

    return $response;
  }

}
