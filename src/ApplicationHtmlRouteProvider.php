<?php

namespace Drupal\contacts_jobs_apps;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Application entities.
 *
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class ApplicationHtmlRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($withdraw_route = $this->getWithdrawFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.withdraw_form", $withdraw_route);
    }

    return $collection;
  }

  /**
   * Returns a route for the Withdraw form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getWithdrawFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('withdraw-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('withdraw-form'));
      $route
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.withdraw",
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.withdraw")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

}
