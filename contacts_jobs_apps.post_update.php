<?php

/**
 * @file
 * Post update hooks for Contacts Jobs Applications.
 */

use Drupal\Core\Site\Settings;

/**
 * Populate the name and email on existing applications.
 */
function contacts_jobs_apps_post_update_populate_email_name(&$sandbox) {
  $app_storage = \Drupal::entityTypeManager()->getStorage('cj_app');

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['last_id'] = 0;
    $sandbox['limit'] = Settings::get('entity_update_batch_size', 50);
    $sandbox['max'] = $app_storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  $ids = $app_storage
    ->getQuery()
    ->accessCheck(FALSE)
    ->condition('id', $sandbox['last_id'], '>')
    ->sort('id', 'ASC')
    ->range(0, $sandbox['limit'])
    ->execute();
  /** @var \Drupal\contacts_jobs_apps\Entity\Application $app */
  foreach ($app_storage->loadMultiple($ids) as $app) {
    $sandbox['last_id'] = $app->id();
    $sandbox['progress']++;

    $user = $app->getOwner();
    if (!$user) {
      continue;
    }

    $app->set('email', $user->getEmail());

    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    if ($profile = $user->get('profile_crm_indiv')->entity) {
      if ($app->hasField('name')) {
        if ($profile->hasField('crm_name')) {
          $app->set('name', $profile->get('crm_name')->getValue());
        }
      }
    }

    $app->save();
  }

  $sandbox['#finished'] = empty($sandbox['max']) || empty($ids) ? 1 : ($sandbox['progress'] / $sandbox['max']);
}
