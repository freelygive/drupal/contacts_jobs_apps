<?php

/**
 * @file
 * Hooks and documentation related to Contacts Jobs Applications.
 */

/**
 * Acts before creating a new Application entity.
 *
 * @param \Drupal\Core\Entity\EntityStorageInterface $storage_controller
 *   The entity storage object.
 * @param mixed[] $values
 *   An array of values to set, keyed by property name. If the entity type has
 *   bundles the bundle key has to be specified.
 *
 * @ingroup entity_crud
 */
function hook_cj_app_pre_create(\Drupal\Core\Entity\EntityStorageInterface $storage_controller, array &$values) {
  $values['email'] = 'test@example.com';
}
