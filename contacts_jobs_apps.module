<?php

/**
 * @file
 * Module for for Jobboard Applications.
 */

use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\contacts_jobs_apps\Entity\Application;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\user\UserInterface;

/**
 * Implements hook_entity_type_build().
 */
function contacts_jobs_apps_entity_type_build(array &$entity_types) {
  /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
  $entity_types['contacts_job']->setLinkTemplate('apply', '/job/{contacts_job}/apply');
}

/**
 * Implements hook_theme().
 */
function contacts_jobs_apps_theme($existing, $type, $theme, $path) {
  return [
    'contacts_jobs_apps_message' => ['render element' => 'content'],
    'cj_app' => ['render element' => 'content'],
  ];
}

/**
 * Implements hook_contacts_user_dashboard_local_tasks_allowed_alter().
 */
function contacts_jobs_apps_contacts_user_dashboard_local_tasks_allowed_alter(&$item_list) {
  // @todo Add applications tab to candidate dashboard.
  $item_list = array_merge($item_list, [
    'contacts_jobs_apps.user.job_apps',
  ]);
}

/**
 * Implements hook_contacts_user_dashboard_user_summary_blocks_alter().
 */
function contacts_jobs_apps_contacts_user_dashboard_user_summary_blocks_alter(&$content, UserInterface $user) {
  if ($user->hasRole('crm_indiv')) {
    $content['apps'] = [
      '#type' => 'user_dashboard_summary',
      '#title' => 'Your applications',
      '#buttons' => [
        [
          'text' => t('Manage'),
          'route_name' => 'contacts_jobs_apps.user.job_apps',
          'route_parameters' => [
            'user' => $user->id(),
          ],
        ],
      ],
      '#weight' => 15,
    ];

    $parent_org_view_block = \Drupal::service('plugin.manager.block')->createInstance('views_block:user_dashboard_job_apps-jobs_summary');
    $block_content = $parent_org_view_block->build();
    $content['apps']['#content'] = [
      '#name' => 'contacts_jobs_apps',
      '#theme' => 'block',
      '#attributes' => [],
      '#configuration' => $parent_org_view_block->getConfiguration(),
      '#plugin_id' => $parent_org_view_block->getPluginId(),
      '#base_plugin_id' => $parent_org_view_block->getBaseId(),
      '#derivative_plugin_id' => $parent_org_view_block->getDerivativeId(),
      '#weight' => $parent_org_view_block->getConfiguration()['weight'] ?? 0,
      'content' => $block_content,
    ];
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for state_machine_transition_form.
 */
function contacts_jobs_apps_form_state_machine_transition_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // For team application state transitions, some are only available for staff,
  // others are automated and not available for any users to invoke.
  /** @var \Drupal\state_machine\Form\StateTransitionForm $form_object */
  $form_object = $form_state->getFormObject();
  $entity = $form_object->getEntity();
  if ($entity instanceof Application) {
    $allowed_transitions = $entity->getManuallyAllowedTransitions(\Drupal::currentUser());
    foreach (Element::children($form['actions']) as $delta) {
      if (!in_array($delta, $allowed_transitions)) {
        unset($form['actions'][$delta]);
      }
    }

    // Always hide delete.
    unset($form['actions']['delete']);
  }
}

/**
 * Implements hook_preprocess_HOOK() for contacts_job.
 */
function contacts_jobs_apps_preprocess_contacts_job(&$variables) {
  /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
  $job = $variables['elements']['#contacts_job'];
  $variables['apply_url'] = [];
  $cacheability = new CacheableMetadata();
  $cacheability->addCacheableDependency($job);

  // Only show the link the job is published.
  if ($job->isPublished()) {
    // Show the link if the user is anonymous or they have access.
    $account = \Drupal::currentUser();
    $access_handler = \Drupal::entityTypeManager()->getAccessControlHandler('cj_app');
    $access_result = $access_handler->createAccess(NULL, $account, [], TRUE);
    $cacheability->addCacheContexts(['user.roles:anonymous']);
    $cacheability->addCacheableDependency($access_result);
    if ($access_result->isAllowed() || $account->isAnonymous()) {
      $url = $job->toUrl('apply')->toString(TRUE);
      $variables['apply_url']['#markup'] = $url->getGeneratedUrl();
      $cacheability->addCacheableDependency($url);
    }
  }

  // Set the cacheability data.
  $cacheability->applyTo($variables['apply_url']);
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function contacts_jobs_apps_cj_app_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  assert($entity instanceof Application);

  // If the application is being viewed in full by a recruiter of the
  // relevant organisation, apply the received transition.
  if ($view_mode === 'full') {
    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $entity->getOrg()->get('group')->entity;
    $member = $group->getMember(\Drupal::currentUser());
    if ($member && $member->hasPermission('manage organisation jobs')) {
      /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
      $state = $entity->get('state')->first();
      $transitions = $state->getTransitions();
      if (isset($transitions['receive'])) {
        $state->applyTransition($transitions['receive']);
        $entity->save();
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_access() for file.
 */
function contacts_jobs_apps_file_access(EntityInterface $entity, $operation, AccountInterface $account) {
  assert($entity instanceof FileInterface);
  $result = AccessResult::neutral();

  // We're only interested in the view/download operations.
  if (!in_array($operation, ['view', 'download'])) {
    return $result;
  }

  // Allow access if the user have view or update access to any applications the
  // file is used on.
  $usage = \Drupal::service('file.usage')->listUsage($entity);
  if (empty($usage['file']['cj_app'])) {
    return $result;
  }

  foreach (Application::loadMultiple(array_keys($usage['file']['cj_app'])) as $app) {
    $result = $result
      ->orIf($app->access('view', $account, TRUE))
      ->orIf($app->access('update', $account, TRUE));
  }

  return $result;
}

/**
 * Implements hook_entity_operation_alter().
 */
function contacts_jobs_apps_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if ($entity->getEntityTypeId() === 'contacts_job') {
    /** @var \Drupal\contacts_jobs\Entity\JobInterface $entity */

    if ($org = $entity->get('organisation')->target_id) {
      /** @var \Drupal\contacts_jobs\Entity\Job $entity */
      $url = Url::fromRoute('contacts_jobs_dashboard.user.job.applications', [
        'user' => $entity->get('organisation')->target_id,
        'contacts_job' => $entity->id(),
      ]);

      // Add View Applications in the top.
      $operations['view_applications'] = [
        'title' => t('View Applications'),
        'weight' => -1,
        'url' => $url,
      ];
    }
  }
}

/**
 * Implements hook_contacts_job_update().
 */
function contacts_jobs_apps_contacts_job_update(JobInterface $entity) {
  // If the job has been unpublished as part of an update (either manual or via
  // code), we want to trigger application expiration, as depending on the
  // exact publish end and the last/next cron run, ExpireApplications could miss
  // it.
  $now = \Drupal::time()->getRequestTime();
  $publish_end = $entity->getPublishEndTime();

  // Nothing to do if the publish end is in the future. NULL will equate to less
  // than now, so that will behave correctly.
  if ($publish_end > $now) {
    return;
  }

  // Nothing to do if the publish end has not changed.
  /** @var \Drupal\contacts_jobs\Entity\JobInterface $original */
  $original = $entity->original;
  if ($original->getPublishEndTime() == $publish_end) {
    return;
  }

  // Queue up an application expiry worker to clean up any applications. We
  // handle anything between the new publish end and now in case it was set
  // significantly in the past and will need the submitted application clean up.
  \Drupal::queue('contacts_jobs_apps_expire')
    ->createItem([
      DrupalDateTime::createFromTimestamp($publish_end),
      DrupalDateTime::createFromTimestamp($now),
    ]);
}
